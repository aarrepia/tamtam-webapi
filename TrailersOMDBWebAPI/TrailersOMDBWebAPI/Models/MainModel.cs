﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TrailersOMDBWebAPI.Models
{
    public class MainModel
    {
        public YoutubeModel youtube { get; set; }
        public OmdbModel omdb { get; set; }
    }
}