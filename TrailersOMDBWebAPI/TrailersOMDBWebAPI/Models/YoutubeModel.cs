﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TrailersOMDBWebAPI.Models
{
    public class YoutubeModel
    {
        public string kind { get; set; }
        public string etag { get; set; }
        public pageInfo pageInfo { get; set; }
        public items[] items { get; set; }
    }

    public class pageInfo
    {
        public int resultsPerPage { get; set; }
        public long totalResults { get; set; }
    }

    public class items
    {
        public string kind { get; set; }
        public string etag { get; set; }
        public id id { get; set; }
    }

    public class id
    {
        public string kind { get; set; }
        public string videoId { get; set; }
    }
}

