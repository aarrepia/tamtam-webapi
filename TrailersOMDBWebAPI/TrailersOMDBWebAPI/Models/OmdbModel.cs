﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TrailersOMDBWebAPI.Models
{
    public class OmdbModel
    {
        public string Title { get; set; }
        public string Year { get; set; }
        public string MyProperty { get; set; }
        public string Poster { get; set; }
        public string imdbRating { get; set; }
        public string Type { get; set; }
        public string Plot { get; set; }
    }
}