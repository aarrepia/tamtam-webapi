﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using TrailersOMDBWebAPI.Models;

namespace TrailersOMDBWebAPI.Controllers
{
    public class YoutubeController : ApiController
    {
               

        public YoutubeModel GetVideoID(string movieTitle, string YoutubeKey)
        {
            movieTitle = movieTitle.Replace(" ", "+");

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://www.googleapis.com/youtube/v3/search");

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

            // List data response.
            HttpResponseMessage response = client.GetAsync("?part=snippet&q="+movieTitle+"&maxResults=1&type=video&key="+ YoutubeKey).Result;  // Blocking call!
            if (response.IsSuccessStatusCode)
            {
                /* THIS IS WORKING
                var data = response.Content.ReadAsStringAsync().Result;
                var myObject = JsonConvert.DeserializeObject(data);
                return Ok(new[] { myObject });
                */

                // BETTER IMPLEMENTATION
                var data = response.Content.ReadAsStringAsync().Result;
                YoutubeModel myObject = (YoutubeModel)JsonConvert.DeserializeObject(data, typeof(YoutubeModel));
                return   myObject ;

            }
            else
            {
                return null;
            }
        }

    }
}
