﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using TrailersOMDBWebAPI.Models;

namespace TrailersOMDBWebAPI.Controllers
{
    public class OmdbController : ApiController
    {
        public OmdbModel GetOMDBRating(string movieTitle)
        {
            movieTitle = movieTitle.Replace(" ", "+");

            // Request to OMDBAPI
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://www.omdbapi.com/");

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = client.GetAsync("?t="+movieTitle+"&y=&plot=full").Result;
            if (response.IsSuccessStatusCode)
            {
                var data = response.Content.ReadAsStringAsync().Result;
                OmdbModel myObject = (OmdbModel)JsonConvert.DeserializeObject(data,typeof(OmdbModel));
                return myObject;
            }
            else
            {
                return null;
            }
        }
    }
}
