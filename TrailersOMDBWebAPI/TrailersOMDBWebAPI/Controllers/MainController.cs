﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TrailersOMDBWebAPI.Models;

namespace TrailersOMDBWebAPI.Controllers
{
    public class MainController : ApiController
    {
        
        public MainModel Get(string movieTitle, string YoutubeKey)
        {
            string movieTitleYoutube = movieTitle + " trailer";
            //Gets the information of the videoID
            YoutubeController youtube = new YoutubeController ();
            YoutubeModel videoID = youtube.GetVideoID(movieTitleYoutube, YoutubeKey);

            //Gets the information about the movie from OMDB
            OmdbController omdb = new OmdbController();
            OmdbModel omdbInfo = omdb.GetOMDBRating(movieTitle);

            MainModel movieInfo = new MainModel();

            movieInfo.youtube = videoID;
            movieInfo.omdb = omdbInfo;

            return movieInfo;
        }
        
        

        
    }
}
